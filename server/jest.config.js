module.exports = {
  testPathIgnorePatterns: ['./node_modules/'],
  verbose: true,
  automock: false,
  coveragePathIgnorePatterns: ['/node_modules/'],
  testRegex: '__tests?__.*.test.js',
  setupFiles: ['./jest.setup.js'],
};
