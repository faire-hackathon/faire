import { GraphQLServer } from 'graphql-yoga';
import dotenv from 'dotenv';
import resolvers from './resolvers';

const ENV = process.env.NODE_ENV || process.argv[2] || 'development';
const isProd = ENV === 'production';

if (!isProd) {
  const result = dotenv.config();
  if (result.error) {
    throw result.error;
  }
}

const server = new GraphQLServer({
  typeDefs: './src/schema.graphql',
  resolvers,
});

const options = {
  port: process.env.PORT,
  endpoint: process.env.SERVER_ENDPOINT,
  subscriptions: '/subscriptions',
  playground: '/playground',
};

server.start(options, ({ port }) => {
  console.log(`Server is running on http://localhost:${port}`);
});
