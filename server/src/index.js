const ENV = process.env.NODE_ENV || process.argv[2] || 'development';

if (ENV === 'production') {
  require('babel-polyfill');
}

require('@babel/register')();
require('./server');
