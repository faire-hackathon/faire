import AxiosMockAdapter from 'axios-mock-adapter';
import Api from '../api';

const API_HOST = process.env.FAIRE_API_HOST;
const API_KEY = process.env.FAIRE_API_TOKEN;
const API_TIMEOUT = process.env.FAIRE_API_TIMEOUT;
const PRODUCT_RESOURCE = '/product';

describe('Api', () => {
  const apiInstance = Api.getInstance();
  const apiMock = new AxiosMockAdapter(apiInstance, { delayResponse: 100 });

  it('should add default baseUrl to Api', () => {
    expect(apiInstance.defaults.baseURL).toEqual(API_HOST);
  });

  it('should add default timeout to Api', () => {
    expect(apiInstance.defaults.timeout).toEqual(API_TIMEOUT);
  });

  it('should add API_KEY to Api', () => {
    expect(apiInstance.defaults.headers['X-FAIRE-ACCESS-TOKEN']).toEqual(API_KEY);
  });

  it('should make an async GET request successfully', async () => {
    apiMock.onGet(`${API_HOST}${PRODUCT_RESOURCE}`).reply(200, [{ success: true }]);

    const response = await Api.read(PRODUCT_RESOURCE);

    expect(response.data).toEqual([{ success: true }]);
  });

  it('should return NULL on GET request error', async () => {
    apiMock.onGet(`${API_HOST}${PRODUCT_RESOURCE}`).networkError();

    const response = await Api.read(PRODUCT_RESOURCE);

    expect(response).toBeNull();
  });

  it('should make an async POST request successfully', async () => {
    const product = { name: "Faire's fantastic candle" };

    apiMock.onPost(`${API_HOST}${PRODUCT_RESOURCE}`, product).reply(200, {
      id: 1,
      name: "Faire's fantastic candle",
    });

    const response = await Api.add(PRODUCT_RESOURCE, product);

    expect(response.data).toEqual({ id: 1, name: "Faire's fantastic candle" });
  });

  it('should return NULL on POST request error', async () => {
    const product = { name: "Faire's fantastic candle" };

    apiMock.onPost(`${API_HOST}${PRODUCT_RESOURCE}`, product).networkError();

    const response = await Api.add(PRODUCT_RESOURCE, product);

    expect(response).toBeNull();
  });

  it('should make an async PUT request successfully', async () => {
    const product = { name: "Faire's fantastic candle" };

    apiMock.onPut(`${API_HOST}${PRODUCT_RESOURCE}`, product).reply(200, {
      id: 1,
      name: "Faire's fantastic candle",
    });

    const response = await Api.update(PRODUCT_RESOURCE, product);

    expect(response.data).toEqual({ id: 1, name: "Faire's fantastic candle" });
  });

  it('should return NULL on PUT request error', async () => {
    const product = { name: "Faire's fantastic candle" };

    apiMock.onPut(`${API_HOST}${PRODUCT_RESOURCE}`, product).networkError();

    const response = await Api.update(PRODUCT_RESOURCE, product);

    expect(response).toBeNull();
  });
});
