import axios from 'axios';

class Api {
  /**
   * Fetch data on api.
   *
   * @async
   * @param {string} resource - The resource's uri
   * return {object} response
   */
  async read(resource) {
    try {
      return await this.getInstance().get(resource);
    } catch (error) {
      this.errorHandler(error);
      return null;
    }
  }

  /**
   * Inserts data on api.
   *
   * @async
   * @param {string} resource - The resource's uri
   * @param {object} data - data to be inserted
   * return {object} response
   */
  async add(resource, data) {
    try {
      return await this.getInstance().post(resource, data);
    } catch (error) {
      this.errorHandler(error);
      return null;
    }
  }

  /**
   * Updates data on api.
   *
   * @async
   * @param {string} resource - The resource's uri
   * @param {object} data - data to be updated
   * return {object} response
   */
  async update(resource, data) {
    try {
      return await this.getInstance().put(resource, data);
    } catch (error) {
      this.errorHandler(error);
      return null;
    }
  }

  getInstance() {
    if (!this.instance) {
      this.instance = axios.create({
        baseURL: process.env.FAIRE_API_HOST,
        headers: { 'X-FAIRE-ACCESS-TOKEN': process.env.FAIRE_API_TOKEN },
        timeout: process.env.FAIRE_API_TIMEOUT,
      });
    }
    return this.instance;
  }

  errorHandler(err) {
    if (err && err.response) {
      let statusCode = err.response.status;
      let message = err.response.data;

      if (message && message.includes('Unauthorized')) {
        statusCode = 401;
        message = 'Invalid API_KEY';
      }

      console.log(` Error: ${statusCode}, ${message}!\n`);
    }
  }
}

export default new Api();
