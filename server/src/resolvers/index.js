import Api from '../helpers/api';
import { categoryNewResource, makersWithFiltersResource } from '../api-resources';

const resolvers = {
  Query: {
    categoryNew: async () => {
      const response = await Api.read(categoryNewResource);
      if (response && response.data) {
        return response.data;
      }
      return [];
    },
    makersWithFilters: async (parent, args) => {
      const data = {
        maker_minimum_thresholds: [10000, 20000, 30000],
        maker_values: [],
        states: [],
        pagination_data: { page_number: 1, page_size: 60 },
        ...args,
      };

      const response = await Api.add(makersWithFiltersResource, data);
      if (response && response.data) {
        let makersFiltered = {
          brands: [],
          pagination_data: {},
        };

        if (response.data.brands && response.data.pagination_data) {
          makersFiltered.brands = response.data.brands;
          makersFiltered.pagination_data = response.data.pagination_data;
        }

        return makersFiltered;
      }

      return {};
    },
  },
};

export default resolvers;
