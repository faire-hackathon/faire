# Faire GraphQL API

This application is a GraphQL wrapper for Faire REST API.
The main idea behind this project is not to re-write the whole REST server but instead, create a mechanism that allows clients such as Mobile devices and Apps to load the necessary information much faster.

The Faire GraphQL API uses the Fair REST API to retrieve pieces of information and provide them to GraphQL clients.

## > [Try it out](https://faire-gql.herokuapp.com/playground)
```
query {
  makersWithFilters {
    brands {
      token
      squared_image {
        token
        url
      }
      name
      minimum_order_amount_cents
      accepted_terms
      active
      active_products_count
      allows_scheduled_orders
    }
    pagination_data {
      page_count
      page_number
      page_size
      total_results
    }
  }
}
```

## Install
```
npm install
```

## Run dev mode
Create a `.env` file into the server root folder, and add the same variables of the `.env.test` file, replace the values according to your environment.

```
npm run dev
```

## Run prod mode
```
npm start
```

## Test
```
npm run test
```

## Test coverage
```
npm run test:cov
```

## GraphQL playground
```
http://localhost:<PORT>/playground
```
