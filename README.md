# Faire site using GraphQL
The goal of this project is to create navigation similar to the Faire's pages, using GraphQL to reduce the needed payloads.

## > [Live Demo](http://faire-gql.surge.sh)

## [GraphQL Server](https://gitlab.com/faire-hackathon/faire/tree/master/server)
For more informations about the GraphQL Server, **[Click here](https://gitlab.com/faire-hackathon/faire/tree/master/server)**.

## Requests size reduction

### Before:

By opening https://www.faire.com/category/All the request to load all the makers (https://www.faire.com/api/search/makers-with-filters) is transfering **101kb**.

### After

By opening http://faire-gql.surge.sh/All%20Products the query to load all the makers is transfering **17.3kb**.


## Install
```
yarn
```

## Run
```
yarn start
```

## Run unit tests
```
yarn test
```

## Authors

[Victor Miguez](https://github.com/victormiguez)

[Rafael Madureira](https://github.com/madureira)
