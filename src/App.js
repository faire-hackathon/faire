import React, { Component } from 'react';
import { ApolloProvider } from 'react-apollo';
import { client } from './ApolloSetup';

import GlobalStyles from './GlobalStyles';
import Routes from './Routes';

class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <GlobalStyles />
        <Routes />
      </ApolloProvider>
    );
  }
}

export default App;
