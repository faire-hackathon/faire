import React from 'react';
import { Global, css } from '@emotion/core';

const GlobalStyles = () => (
  <Global
    styles={css`
      body {
        font-family: 'Raleway', sans-serif;
        max-width: 1680px;
        margin: 0 auto;
      }
    `}
  />
);

export default GlobalStyles;
