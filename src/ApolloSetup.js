import ApolloClient from 'apollo-boost';

export const client = new ApolloClient({
  uri: 'https://faire-gql.herokuapp.com/',
});
