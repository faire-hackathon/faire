import React from 'react';
import * as styles from './MakersList.styles';

const MakersList = ({ children }) => {
  return <section className={styles.list}>{children}</section>;
};

export default MakersList;
