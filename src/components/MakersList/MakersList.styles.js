import { css } from 'emotion';

export const list = css`
  display: flex;
  flex-wrap: wrap;
`;
