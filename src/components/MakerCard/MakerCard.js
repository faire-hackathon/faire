import React from 'react';
import * as styles from './MakerCard.styles';

const MakerCard = ({ brand }) => {
  return (
    <article className={styles.card}>
      {brand.squared_image && (
        <img
          src={brand.squared_image.url}
          alt={`${brand.name} featured product`}
          className={styles.image}
        />
      )}

      <div className={styles.description}>
        <h6 className={styles.brandName}>{brand.name}</h6>
        <span className={styles.price}>
          ${brand.minimum_order_amount_cents / 100} Minimum
        </span>
      </div>
    </article>
  );
};

export default MakerCard;
