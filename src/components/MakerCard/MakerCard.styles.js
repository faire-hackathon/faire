import { css } from 'emotion';

export const card = css`
  width: calc(20% - 32px);
  margin: 16px;
`;

export const image = css`
  width: 100%;
  display: block;
`;

export const description = css`
  border: 1px solid #dfe3e8;
  border-top: 0;
  text-align: center;
  padding: 8px;
`;

export const brandName = css`
  margin: 0;
  font-size: 18px;
  margin-bottom: 4px;
  color: #454f5b;
`;

export const price = css`
  font-size: 14px;
  color: #919eab;
`;
