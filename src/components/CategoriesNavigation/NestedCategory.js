import React from 'react';
import { Link } from 'react-router-dom';
import { cx } from 'emotion';

import * as styles from './CategoriesNavigation.styles';

const NestedCategory = ({
  category,
  currentCategory,
  parentCategory,
  renderCategory,
  changeNestedCategoryParent,
  changeNestedCategory,
  nestingLevel,
}) => {
  return (
    <li
      className={cx(styles.navigationCategory, {
        [styles.selectedCategory]:
          currentCategory === category.name || parentCategory === category.name,
      })}
      onClick={() => changeNestedCategoryParent(category.name)}
    >
      <Link to={`/${category.name}`}>{category.name}</Link>

      {renderCategory(category['sub_categories'], nestingLevel + 1)}
    </li>
  );
};

export default NestedCategory;
