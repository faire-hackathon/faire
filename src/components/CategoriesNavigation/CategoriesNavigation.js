import React, { Component } from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';

import SingleCategory from './SingleCategory';
import NestedCategory from './NestedCategory';
import * as styles from './CategoriesNavigation.styles';

export const GET_CATEGORIES_QUERY = gql`
  query {
    categoryNew {
      name
      sub_categories {
        name
        sub_categories {
          name
          sub_categories {
            name
          }
        }
      }
    }
  }
`;

class CategoriesNavigation extends Component {
  state = {
    parentCategory: null,
  };

  changeSingleCategory = category => {
    this.setState({ parentCategory: null });
  };

  changeNestedCategoryParent = parentCategory => {
    this.setState({ parentCategory });
  };

  renderCategory = (categories, nestingLevel = 0) => {
    return (
      <ul className={styles.categoriesList(nestingLevel)}>
        {categories.map(category => {
          const { parentCategory } = this.state;
          const { currentCategory } = this.props;

          if (category.sub_categories.length === 0) {
            return (
              <SingleCategory
                key={category.name}
                category={category}
                currentCategory={currentCategory}
                changeSingleCategory={this.changeSingleCategory}
              />
            );
          }

          return (
            <NestedCategory
              key={category.name}
              category={category}
              currentCategory={currentCategory}
              parentCategory={parentCategory}
              renderCategory={this.renderCategory}
              changeNestedCategoryParent={this.changeNestedCategoryParent}
              nestingLevel={nestingLevel}
            />
          );
        })}
      </ul>
    );
  };

  render() {
    return (
      <Query query={GET_CATEGORIES_QUERY}>
        {({ loading, error, data }) => {
          if (loading) return null;

          return (
            <div className={styles.categoriesWrapper}>
              {this.renderCategory(data.categoryNew)}
            </div>
          );
        }}
      </Query>
    );
  }
}

export default CategoriesNavigation;
