import React from 'react';
import { MemoryRouter } from 'react-router';
import { mount } from 'enzyme';

import CategoriesNavigation, {
  GET_CATEGORIES_QUERY,
} from './CategoriesNavigation';

import { MockedProvider } from 'react-apollo/test-utils';
import wait from 'waait';

const mocks = [
  {
    request: {
      query: GET_CATEGORIES_QUERY,
    },
    result: {
      data: {
        categoryNew: [
          {
            name: 'Foo',
            sub_categories: [],
          },
          {
            name: 'Foo bar',
            sub_categories: [
              {
                name: 'Foo baz',
                sub_categories: [],
              },
              {
                name: 'Foo bax',
                sub_categories: [],
              },
            ],
          },
        ],
      },
    },
  },
];

describe('<CategoriesNavigation />', () => {
  it('should render the correct amount of components', async () => {
    const component = mount(
      <MockedProvider mocks={mocks} addTypename={false}>
        <MemoryRouter>
          <CategoriesNavigation currentCategory="Foo" />
        </MemoryRouter>
      </MockedProvider>
    );

    await wait(0);
    component.update();

    expect(component.find('SingleCategory')).toHaveLength(3);
    expect(component.find('NestedCategory')).toHaveLength(1);
  });
});
