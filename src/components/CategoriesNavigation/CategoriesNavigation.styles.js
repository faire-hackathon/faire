import { css } from 'emotion';

export const categoriesWrapper = css`
  min-width: 200px;
  max-width: 200px;
  flex-basis: auto;
  flex-grow: 1;
`;

export const categoriesList = nestingLevel => css`
  list-style: none;
  padding: 0;
  padding-left: ${8 * nestingLevel}px;
`;

export const navigationCategory = css`
  font-weight: 400;
  cursor: pointer;
  padding: 4px;
  margin-bottom: 4px;

  ul {
    display: none;
  }

  a {
    color: #454f5b;
    display: block;
    text-decoration: none;
  }
`;

export const selectedCategory = css`
  font-weight: 600;

  ul {
    display: block;
  }
`;
