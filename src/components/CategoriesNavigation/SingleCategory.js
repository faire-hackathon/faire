import React from 'react';
import { Link } from 'react-router-dom';
import { cx } from 'emotion';

import * as styles from './CategoriesNavigation.styles';

const SingleCategory = ({
  category,
  currentCategory,
  changeSingleCategory,
}) => {
  return (
    <li
      className={cx(styles.navigationCategory, {
        [styles.selectedCategory]: currentCategory === category.name,
      })}
      onClick={() => changeSingleCategory()}
    >
      <Link to={`/${category.name}`}>{category.name}</Link>
    </li>
  );
};

export default SingleCategory;
