import { css } from 'emotion';

export const wrapper = css`
  display: flex;
`;

export const categoryDescription = css`
  color: #454f5b;
  padding: 0 16px;
`;

export const spinner = css`
  display: block;
  margin: 20px auto;
  width: 30px;
`;
