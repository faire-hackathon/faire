import React, { Component } from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';

import MakerCard from 'src/components/MakerCard/MakerCard';
import MakersList from 'src/components/MakersList/MakersList';

import * as styles from './Makers.syles';

import loader from 'src/assets/loader.svg';

export const GET_MAKERS_QUERY = gql`
  query makersWithFilters($category: String) {
    makersWithFilters(category: $category) {
      brands {
        token
        squared_image {
          url
        }
        name
        minimum_order_amount_cents
      }
      pagination_data {
        total_results
      }
    }
  }
`;

class Makers extends Component {
  state = {
    isLoadingMakers: true,
    makersList: [],
    requestError: null,
  };

  render() {
    const { match } = this.props;

    return (
      <Query
        query={GET_MAKERS_QUERY}
        variables={{ category: match.params.category }}
      >
        {({ loading, error, data }) => {
          if (loading)
            return (
              <img src={loader} className={styles.spinner} alt="Spinner" />
            );

          if (error) return `Error! ${error.message}`;

          const { brands, pagination_data } = data.makersWithFilters;

          return (
            <section>
              <header className={styles.categoryDescription}>
                <h2>{this.props.match.params.category}</h2>
                <h3>{pagination_data.total_results} Results</h3>
              </header>
              <MakersList>
                {brands.map(brand => (
                  <MakerCard key={brand.token} brand={brand} />
                ))}
              </MakersList>
            </section>
          );
        }}
      </Query>
    );
  }
}

export default Makers;
