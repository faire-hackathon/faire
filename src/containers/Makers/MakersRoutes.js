import React from 'react';

import { Route } from 'react-router-dom';
import Makers from './Makers';

import CategoriesNavigation from 'src/components/CategoriesNavigation/CategoriesNavigation';
import * as styles from './Makers.syles';

const MakersRoutes = () => (
  <Route
    path="/:category?"
    render={props => (
      <section className={styles.wrapper}>
        <CategoriesNavigation currentCategory={props.match.params.category} />
        <Makers {...props} key={props.match.params.category} />
      </section>
    )}
  />
);

export default MakersRoutes;
