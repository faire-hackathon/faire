import React from 'react';
import { mount } from 'enzyme';
import { MockedProvider } from 'react-apollo/test-utils';
import wait from 'waait';

import Makers, { GET_MAKERS_QUERY } from './Makers';

const mocks = [
  {
    request: {
      query: GET_MAKERS_QUERY,
      variables: {
        category: 'Foo',
      },
    },
    result: {
      data: {
        makersWithFilters: {
          brands: [
            {
              token: '1',
              name: 'Foo',
              minimum_order_amount_cents: 100,
              squared_image: { url: 'https://foo.com/image.jpg' },
            },
          ],
          pagination_data: { total_results: 1 },
        },
      },
    },
  },
];

const props = {
  match: {
    params: {
      category: 'Foo',
    },
  },
};

describe('<Makers />', () => {
  it('should show a spinner during the request', () => {
    const component = mount(
      <MockedProvider mocks={[]}>
        <Makers {...props} />
      </MockedProvider>
    );

    expect(component.find('img')).toHaveLength(1);
  });

  it('should show maker card after load data', async () => {
    const component = mount(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Makers {...props} />
      </MockedProvider>
    );

    await wait(0);
    component.update();

    expect(component.find('MakerCard')).toHaveLength(1);
  });
});
