import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

import MakersRoutes from 'src/containers/Makers/MakersRoutes';

const Routes = () => (
  <Router>
    <MakersRoutes />
  </Router>
);

export default Routes;
